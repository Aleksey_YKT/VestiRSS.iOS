//
//  FeedTableViewController.swift
//  vesti_rss
//
//  Created by Aleksey Ivanov on 31.10.2017.
//  Copyright © 2017 Aleksey Ivanov. All rights reserved.
//

import UIKit

struct News {
    var title: String!
    var link: String!
    var description: String!
    var pubDate: String!
    var category: String!
    var enclosure: String!
    var text: String!
}

class FeedTableViewController: UITableViewController, XMLParserDelegate {

    var tempElement: String?
    var tempEnclosure: String?
    var tempEnclosureType: String?
    var tempPost: News? = nil
    var newsFeed: [News] = []
    var parser = XMLParser()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getNews()
        
        self.refreshControl?.addTarget(self, action: #selector(FeedTableViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func getNews() {
        newsFeed = []
        tempPost = nil
        tempEnclosure = nil
        tempElement = nil
        
        OperationQueue.main.addOperation({ () -> Void in
            self.parser = XMLParser(contentsOf: URL(string: "http://www.vesti.ru/vesti.rss")!)!
            self.parser.delegate = self
            self.parser.parse()
            
            self.tableView.reloadData()
        })
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        OperationQueue.main.addOperation({ () -> Void in
            self.getNews()
            self.tableView.reloadData()
            refreshControl.endRefreshing()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError)
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        tempElement = elementName
        if elementName == "item" {
            tempPost = News(title: "", link: "", description: "", pubDate: "", category: "", enclosure: "", text: "")
        }
        
        if elementName == "enclosure" {
            if let urlString = attributeDict["url"] {
                tempEnclosure = urlString
            }
            
            if let mediaType = attributeDict["type"] {
                tempEnclosureType = mediaType
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if let post = tempPost, let str = string as String? {
            if tempElement == "title" {
                tempPost?.title = post.title+str
            } else if tempElement == "link" {
                tempPost?.link = post.link+str
            } else if tempElement == "description" {
                tempPost?.description = post.description+str
            } else if tempElement == "pubDate" {
                tempPost?.pubDate = post.pubDate+str
            } else if tempElement == "category" {
                tempPost?.category = post.category+str
            } else if tempElement == "enclosure" {
                if tempEnclosureType == "image/jpeg" {
                    tempPost?.enclosure = tempEnclosure
                } else {
                    tempPost?.enclosure = ""
                }
            } else if tempElement == "yandex:full-text" {
                tempPost?.text = post.text+str
            }
            
            tempEnclosure = ""
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            if let post = tempPost {
                newsFeed.append(post)
            }
            
            tempPost = nil
            tempEnclosure = nil
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsFeed.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! DetailsViewController
                destinationController.news = newsFeed[indexPath.row]
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as! FeedTableViewCell
        
        cell.newsTitleTextView?.isUserInteractionEnabled = false
        cell.newsDescriptionTextView?.isUserInteractionEnabled = false
        
        cell.newsCategoryLabel?.text = newsFeed[indexPath.row].category
        cell.newsTitleTextView?.text = newsFeed[indexPath.row].title
        cell.newsDescriptionTextView?.text = newsFeed[indexPath.row].description
        cell.dateLabel?.text = dateFormatter(pubDate: newsFeed[indexPath.row].pubDate)

        var url = ""
        
        if newsFeed[indexPath.row].enclosure != "" {
            url = newsFeed[indexPath.row].enclosure
        } else {
            url = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/320px-No_image_3x4.svg.png"
        }
            
        if let checkedUrl = URL(string: url) {
            getImageFromUrl(url: checkedUrl) { (data, response, error) in guard let data = data, error == nil else  { return }
                DispatchQueue.main.async() { () -> Void in
                    cell.newsImageView?.image = UIImage(data: data)
                }
            }
        }
        
        return cell
    }
    
    func getImageFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
}
