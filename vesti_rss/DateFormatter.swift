//
//  DateFormatter.swift
//  vesti_rss
//
//  Created by Aleksey Ivanov on 31.10.2017.
//  Copyright © 2017 Aleksey Ivanov. All rights reserved.
//

import Foundation

func dateFormatter(pubDate: String) -> String {
    var formattedPubDate = String()
    
    if (pubDate.characters.count > 0) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z\n"
        let dateObj = dateFormatter.date(from: pubDate)
        dateFormatter.dateFormat = "dd MMM в HH:mm"
        formattedPubDate = dateFormatter.string(from: dateObj!)
    }
    
    return formattedPubDate
}
