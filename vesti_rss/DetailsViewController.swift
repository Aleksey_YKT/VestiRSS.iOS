//
//  DetailsViewController.swift
//  vesti_rss
//
//  Created by Aleksey Ivanov on 31.10.2017.
//  Copyright © 2017 Aleksey Ivanov. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var categoryTextView: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTextView: UITextView!
    @IBOutlet weak var linkTextView: UITextView!
    
    var news = News()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleTextView.text = news.title
        dateLabel.text = dateFormatter(pubDate: news.pubDate)
        newsTextView.text = news.text
        categoryTextView.text = news.category
        linkTextView.text = news.link
        
        var url = ""
        
        if news.enclosure != "" {
            url = news.enclosure
        } else {
            url = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/320px-No_image_3x4.svg.png"
        }
        
        if let checkedUrl = URL(string: url) {
            getImageFromUrl(url: checkedUrl) { (data, response, error) in guard let data = data, error == nil else  { return }
                DispatchQueue.main.async() { () -> Void in
                    self.newsImageView?.image = UIImage(data: data)
                }
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    func getImageFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
