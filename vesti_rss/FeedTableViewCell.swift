//
//  FeedTableViewCell.swift
//  vesti_rss
//
//  Created by Aleksey Ivanov on 31.10.2017.
//  Copyright © 2017 Aleksey Ivanov. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet weak var newsCategoryLabel: UILabel!
    @IBOutlet weak var newsTitleTextView: UITextView!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsDescriptionTextView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
